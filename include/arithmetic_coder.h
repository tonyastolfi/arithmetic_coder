#pragma once

#include <vector>
#include <string>


#ifdef _WIN32
  #define ARITHMETIC_CODER_EXPORT __declspec(dllexport)
#else
  #define ARITHMETIC_CODER_EXPORT
#endif

ARITHMETIC_CODER_EXPORT void arithmetic_coder();
ARITHMETIC_CODER_EXPORT void arithmetic_coder_print_vector(const std::vector<std::string> &strings);
